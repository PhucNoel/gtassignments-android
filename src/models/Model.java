package models;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import presenters.LoginPresenter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static models.Constants.WEBROOT;

public class Model implements IModel {

    static public LoginPresenter my_presenter;
    private static Map map;
    protected String my_username;
    protected String my_password;

    public Model(LoginPresenter presenter) {
        my_presenter = presenter;
    }

    @Override
    public void GetLoginInformation(String username, String password) {
        // TODO Auto-generated method stub
        my_username = username;
        my_password = password;
    }

    @Override
    public boolean Authenticate() {

        if (my_username == null || my_password == null) {
            return false;
        } else {
            postObject MyPost = new postObject("common/db_lib/authenticate.php");
            HashMap<String, String> MyParams = new HashMap<String, String>();

            MyParams.put("username", my_username);
            MyParams.put("password", my_password);

            MyPost.setParams(MyParams);

            try {
                MyPost.execute("");
            } catch (Exception e) {
                e.printStackTrace();
            }


            return MyPost.getRetVal();
        }


    }

    public boolean syncCalendar(Context ctx) {
//        CalendarActivity cal = new CalendarActivity();
//        cal.setContext(ctx);
//
//        Iterator itr = (Iterator) map.keySet();
//        while( itr.hasNext() ){
//            long startDate = 0;
//            try {
//                Date dt = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2013-10-29 00:00");
//                startDate = dt.getTime();
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//            CalendarEvent calEvent = new CalendarEvent("testing", null, null, startDate, startDate, 3);
//            cal.addEvent(calEvent);
//        }

        return true;
    }

    class postObject extends AsyncTask<String, Void, Boolean> {

        private String base_url;
        Exception exception = null;
        HashMap<String, ?> my_map;
        private boolean retVal = false;
        private InputStream content;
        private String json = "";
        private JSONObject jObj = null;
        String authenticationStatus = "";
        Map map;

        postObject(String url) {
            base_url = WEBROOT + url;
        }

        @Override
        public Boolean doInBackground(String... arg0) {

            try {

                //Setup the parameters
                ArrayList<NameValuePair> nameValuePairs = parseParams(my_map);

                //Create the HTTP request
                HttpParams httpParameters = new BasicHttpParams();

                //Setup timeouts
                HttpConnectionParams.setConnectionTimeout(httpParameters, 25000);
                HttpConnectionParams.setSoTimeout(httpParameters, 25000);

                HttpClient httpclient = new DefaultHttpClient(httpParameters);
                HttpPost httppost = new HttpPost(base_url);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity httpEntity = response.getEntity();
                content = httpEntity.getContent();

                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            content, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    content.close();
                    json = sb.toString();
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }

                // try parse the string to a JSON object
                try {
                    jObj = new JSONObject(json);
                } catch (JSONException e) {
                    Log.e("JSON Parser", "Error parsing data " + e.toString());
                }

                authenticationStatus = jObj.getString("authentication_status");

                if (authenticationStatus.equals("Successfully")) {
                    ArrayList jsonArray = new ArrayList();
                    Iterator itr = jObj.keys();
                    ArrayList jsonarray = new ArrayList();
                    map = new HashMap();
                    ArrayList<String> assignmentTableHeaders = new ArrayList<String>();
                    assignmentTableHeaders.add(0, "Title");
                    assignmentTableHeaders.add(1, "Status");
                    assignmentTableHeaders.add(2, "Updated");
                    assignmentTableHeaders.add(3, "Open");
                    assignmentTableHeaders.add(4, "Close");
                    while (itr.hasNext()) {
                        String course = (String) itr.next();
                        if (!course.equals("authentication_status")) {

                            ArrayList courseAssignments = new ArrayList();
                            JSONArray assignmentsArray = jObj.getJSONArray(course);

                            for (int i = 0; i < assignmentsArray.length(); i++) {

                                Map assignmentMap = new HashMap();
                                JSONObject assignment = assignmentsArray.getJSONObject(i);

                                for (String k : assignmentTableHeaders) {

                                    assignmentMap.put(k, assignment.getString(k));

                                }

                                courseAssignments.add(assignmentMap);

                            }

                            map.put(course, courseAssignments);
                        }

                    }
                    Model.map = map;
                    my_presenter.setDataMap(map);
                    return true;
                } else {
                    return false;
                }


            } catch (Exception e) {
                Log.e("Server", "Error:", e);
                exception = e;
            }

            return true;
        }

        public void setParams(Map<String, ?> map) {
            my_map = (HashMap<String, ?>) map;

        }

        @Override
        protected void onPreExecute() {
            my_presenter.showProgressBar();
        }

        @Override
        public void onPostExecute(Boolean valid) {
            //Update the UI
            //my_presenter.hideProgressBar();
            my_presenter.updateUI(valid.booleanValue());

        }

        private ArrayList<NameValuePair> parseParams(Map<String, ?> map) {
            ArrayList<NameValuePair> RetPairs = new ArrayList<NameValuePair>();
            Iterator<?> it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                System.out.println(pairs.getKey() + " = " + pairs.getValue());
                RetPairs.add(new BasicNameValuePair((String) pairs.getKey(), (String) pairs.getValue()));
                it.remove(); // avoids a ConcurrentModificationException
            }

            return RetPairs;
        }

        protected Boolean getRetVal() {
            return retVal;
        }
    }
}
