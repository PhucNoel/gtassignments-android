package models;

import android.content.Context;

public interface IModel {

    public void GetLoginInformation(String username, String password);

    public boolean Authenticate();

    public boolean syncCalendar(Context ctx);
}
