package presenters;

import android.content.Context;
import models.IModel;
import models.Model;
import views.ILoginView;

import java.io.Serializable;
import java.util.Map;

public class LoginPresenter implements Serializable {

    private static final long serialVersionUID = -8945694599256970511L;
    private ILoginView my_view;
    private String my_username, my_password;
    private IModel my_model;
    private Map myDataMap;


    public LoginPresenter(ILoginView view) {
        my_view = view;
        my_model = new Model(this);
    }

    public void GetLoginInformation(String username, String password) {
        my_username = username;
        my_password = password;
    }

    public void authenticate(String username, String password) {
        my_model.GetLoginInformation(username, password);

//		if( my_model.Authenticate() )
//		{
//			my_view.advance();
//		}
//		else
//		{
//            hideProgressBar();
//			my_view.login_error();
//		}
        my_model.Authenticate();
    }

    public void showProgressBar() {
        my_view.showProgressBar();
    }

    public void hideProgressBar() {
        my_view.hideProgressBar();
    }

    public void setDataMap(Map map) {
        myDataMap = map;
    }

    public void updateUI(boolean result) {

        if (result) {
            my_view.setData(myDataMap);
            my_view.advance();
        } else {
            my_view.login_error();
        }

    }

    public boolean syncCalendar(Context ctx) {
        my_model.syncCalendar(ctx);
        return true;
    }
}
