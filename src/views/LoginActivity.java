package views;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.prj092713.R;
import org.json.JSONException;
import org.json.JSONObject;
import presenters.LoginPresenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends Activity implements ILoginView {

    private ProgressBar bar;
    private LoginPresenter my_presenter;
    private TableLayout tl;
    private TableRow sample_row;
    private ViewGroup.LayoutParams sample_layout;
    private ViewGroup.LayoutParams titleCellLayout, statusCellLayout, openCellLayout, closeCellLayout;
    private Map dataMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        my_presenter = new LoginPresenter(this);
        bar = (ProgressBar) this.findViewById(R.id.ProgressBar);
        bar.setVisibility(View.GONE);
        bar.setProgress(5);

    }

    public void onClickLogin(View source) throws JSONException {
        String username = getUsername().getText().toString();
        String password = getPassword().getText().toString();

//        CalendarActivity cal = new CalendarActivity();
//        cal.setContext(this.getApplicationContext());
//        long startDate = 0;
//        try {
//            Date dt = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2013-10-30 00:00");
//            startDate = dt.getTime();
//
//        } catch (ParseException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
//        CalendarEvent calEvent = new CalendarEvent("testing", null, null, startDate, startDate, 3);
//        cal.addEvent(calEvent);
        if (username.equals("") || password.equals("")) {
            getUsername().requestFocus();
            getUsername().setError("Please enter login information.");
            Intent home_intent = new Intent(LoginActivity.this, HomePageActivity.class);
            startActivity(home_intent);
        } else {
            my_presenter.authenticate(username, password);
        }

    }

    public void addEvent() {
        long calID = 3;
        long startMillis = 0;
        long endMillis = 0;
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(2013, 10, 28, 7, 30);
        startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance();
        endTime.set(2013, 10, 28, 8, 45);
        endMillis = endTime.getTimeInMillis();

        ContentResolver cr = getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, "Jazzercise");
        values.put(CalendarContract.Events.DESCRIPTION, "Group workout");
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, "America/Los_Angeles");
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        long eventID = Long.parseLong(uri.getLastPathSegment());
    }

    public EditText getUsername() {
        return (EditText) findViewById(R.id.username_text);
    }

    public EditText getPassword() {
        return (EditText) findViewById(R.id.password_text);
    }

    @Override
    public void advance() {
        Context context = getApplicationContext();
        CharSequence text = "Welcome!";

        JSONObject tmp = new JSONObject();
        try {
            tmp.put("testkey", "testvalue");
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        int duration = Toast.LENGTH_SHORT;

        hideProgressBar();
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        Map<String, String[]> myMap = new HashMap<String, String[]>();
        ArrayList<JSONObject> jsonArray = new ArrayList<JSONObject>();


        jsonArray.add(tmp);

        Bundle jsonBundle = new Bundle();
        jsonBundle.putSerializable("map", (Serializable) dataMap);
        Intent home_intent = new Intent(LoginActivity.this, HomePageActivity.class);
        home_intent.putExtra("dataMap", (Serializable) dataMap);
        home_intent.putExtra("bundleMap", jsonBundle);
        startActivity(home_intent);

    }

    public void login_error() {
        Context context = getApplicationContext();
        CharSequence text = "Incorrect username or password.";
        int duration = Toast.LENGTH_SHORT;
        hideProgressBar();
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

    }

    @Override
    public void setData(Map map) {
        dataMap = map;
    }

    @Override
    public void showProgressBar() {
        bar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        bar.setVisibility(View.GONE);
    }

    public void finishActivity() {
        finish();
    }


}
