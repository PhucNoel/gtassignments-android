package views;


import android.content.ContentValues;
import android.provider.CalendarContract;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created with IntelliJ IDEA.
 * User: Phuc Noel
 * Date: 10/28/13
 * Time: 1:01 AM
 */
public class CalendarEvent {
    private String title;
    private String descr;
    private String location;
    private long startTime;
    private long endTime;
    private long idCalendar;

    public CalendarEvent(String title, String descr, String location, long startTime, long endTime, long idCalendar) {
        this.title = title;
        this.descr = descr;
        this.location = location;
        this.startTime = startTime;
        this.endTime = endTime;
        this.idCalendar = idCalendar;
    }


    private long getIdCalendar() {
        return this.idCalendar;
    }

    private String getTitle() {
        return this.title;
    }

    private String getLocation() {
        return this.location;
    }

    private String getDescr() {
        return this.descr;
    }

    private long getStartTime() {
        return this.startTime;
    }

    private long getEndTime() {
        return this.endTime;
    }

    public static ContentValues toContentValues(CalendarEvent evt) {
        ContentValues cv = new ContentValues();
        cv.put("calendar_id", evt.getIdCalendar());
        cv.put("title", evt.getTitle());
        cv.put("description", evt.getDescr());
        cv.put("eventLocation", evt.getLocation());
        cv.put("dtstart", evt.getStartTime());
        cv.put("dtend", evt.getEndTime());
        cv.put("eventStatus", 1);
        cv.put("visibility", 0);
        cv.put("transparency", 0);

        return cv;

    }

    public static ContentValues toICSContentValues(CalendarEvent evt) {

        ContentValues cv = new ContentValues();
        cv.put(CalendarContract.Events.CALENDAR_ID, evt.getIdCalendar());
        cv.put(CalendarContract.Events.TITLE, evt.getTitle());
        cv.put(CalendarContract.Events.DESCRIPTION, evt.getDescr());
        cv.put(CalendarContract.Events.EVENT_LOCATION, evt.getLocation());
        cv.put(CalendarContract.Events.DTSTART, evt.getStartTime());
        cv.put(CalendarContract.Events.DTEND, evt.getEndTime());
        cv.put(CalendarContract.Events.STATUS, 1);
        cv.put(CalendarContract.Events.VISIBLE, 1);
        cv.put("transparency", 0);
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();

        cv.put(CalendarContract.Events.EVENT_TIMEZONE, tz.getDisplayName());
    /*
    cv.put(Events.STATUS, 1);
    cv.put(Events.VISIBLE, 0);
    cv.put("transparency", 0);

    return cv;
    */

        return cv;
    }
}
