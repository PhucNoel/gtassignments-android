package views;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.view.ViewPager;
import android.view.*;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.example.prj092713.R;
import com.squareup.timessquare.CalendarPickerView;
import org.json.JSONObject;
import presenters.LoginPresenter;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class HomePageActivity extends FragmentActivity {
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments representing
     * each object in a collection. We use a {@link android.support.v4.app.FragmentStatePagerAdapter}
     * derivative, which will destroy and re-create fragments as needed, saving and restoring their
     * state in the process. This is important to conserve memory and is a best practice when
     * allowing navigation between objects in a potentially large collection.
     */
    DemoCollectionPagerAdapter mDemoCollectionPagerAdapter;
    LoginPresenter myPresenter;

    /**
     * The {@link android.support.v4.view.ViewPager} that will display the object collection.
     */
    ViewPager mViewPager;
    private JSONObject myJObj;
    private HashMap<String, ArrayList> myMap;
    private AlarmManagerBroadcastReceiver alarm;
    private Calendar nextYear = Calendar.getInstance();
    private Calendar lastYear = Calendar.getInstance();

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        alarm = new AlarmManagerBroadcastReceiver();
        setContentView(R.layout.activity_collection_demo);
        nextYear.add(Calendar.YEAR, 1);

        lastYear.add(Calendar.YEAR, -1);
        // Create an adapter that when requested, will return a fragment representing an object in
        // the collection.
        // 
        // ViewPager and its adapters use support library fragments, so we must use
        // getSupportFragmentManager.
        mDemoCollectionPagerAdapter = new DemoCollectionPagerAdapter(getSupportFragmentManager());

        // Set up action bar.
        final ActionBar actionBar = getActionBar();

        // Specify that the Home button should show an "Up" caret, indicating that touching the
        // button will take the user one step up in the application's hierarchy.
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Set up the ViewPager, attaching the adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mDemoCollectionPagerAdapter);

        myMap = (HashMap) getIntent().getSerializableExtra("dataMap");
        ArrayList<CharSequence> keySet = new ArrayList<CharSequence>(myMap.keySet());

        mDemoCollectionPagerAdapter.setTitles(keySet);
        mDemoCollectionPagerAdapter.setCount(myMap.size());
        mDemoCollectionPagerAdapter.setData(myMap);
        mDemoCollectionPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.login, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed in the action bar.
                // Create a simple intent that starts the hierarchical parent activity and
                // use NavUtils in the Support Package to ensure proper handling of Up.
                Intent upIntent = new Intent(this, LoginActivity.class);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.from(this)
                            // If there are ancestor activities, they should be added here.
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                    // This activity is part of the application's task, so simply
                    // navigate up to the hierarchical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                finish();
                return true;
            case R.id.actionSyncCalendar:

//                AlertDialog.Builder builder = new AlertDialog.Builder(this);
//                builder.setMessage("Do you want to add coming assignments to your calendar?");
//                builder.setCancelable(true);
//                builder.setPositiveButton("No", new CancelOnClickListener());
//                builder.setNegativeButton("Yes", new OkOnClickListener());
//                AlertDialog dialog = builder.create();
//                dialog.show();
                CalendarPickerView dialogView =
                        (CalendarPickerView) getLayoutInflater().inflate(R.layout.dialog, null, false);
                dialogView.init(new Date(), nextYear.getTime());
                new AlertDialog.Builder(HomePageActivity.this)
                        .setTitle("Calendar")
                        .setView(dialogView)
                        .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();
                return true;
            case R.id.actionAddAlarm:
                setAssignmentAlarm();
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            synchronizeCalendar();
            Toast.makeText(getApplicationContext(), "Assignments are added.",
                    Toast.LENGTH_LONG).show();
        }
    }

    private final class CancelOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
        }
    }

    /**
     * A {@link android.support.v4.app.FragmentStatePagerAdapter} that returns a fragment
     * representing an object in the collection.
     */
    public static class DemoCollectionPagerAdapter extends FragmentStatePagerAdapter {

        public static int count = 0;
        public static HashMap<String, ArrayList> map;
        public static ArrayList<CharSequence> myMitles;

        public DemoCollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new DemoObjectFragment();

            Bundle args = new Bundle();
            String courseName = getPageTitle(i).toString();
            ArrayList assignments = map.get(courseName);
            args.putSerializable("assignments", (Serializable) assignments);
            args.putInt(DemoObjectFragment.ARG_OBJECT, i + 1); // Our object is just an integer :-P

            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            // For this contrived example, we have a 100-object collection.
            return count;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            CharSequence title = myMitles.get(position);
            return title;
        }

        public void setCount(int number) {
            count = number;

        }

        public void setTitles(ArrayList<CharSequence> titles) {
            myMitles = titles;
        }

        public void setData(HashMap<String, ArrayList> inMap) {
            map = inMap;
        }

    }

    /**
     * A dummy fragment representing a section of the app, but that simply displays dummy text.
     */
    public static class DemoObjectFragment extends Fragment {

        public static final String ARG_OBJECT = "object";
        public static ArrayList<HashMap> assignments;
        TableRow.LayoutParams rowLayout = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT);

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            TableLayout rootView = (TableLayout) inflater.inflate(R.layout.assignments_table, container, false);
            TableRow sampleRow = (TableRow) rootView.getChildAt(0);
            rowLayout.weight = 1.0f;
            rowLayout.gravity = Gravity.CENTER_HORIZONTAL;
            rowLayout.height = TableRow.LayoutParams.WRAP_CONTENT;

            Bundle args = getArguments();
            assignments = (ArrayList) args.getSerializable("assignments");

            for (HashMap<String, String> key : assignments) {
                TableRow row = new TableRow(getActivity());
                row.setLayoutParams(rowLayout);

                TextView text1 = new TextView(getActivity());
                text1.setText(key.get("Title"));
                text1.setGravity(Gravity.CENTER);
                row.addView(text1);
                TextView text2 = new TextView(getActivity());
                text2.setText(key.get("Status"));
                text2.setGravity(Gravity.CENTER);
                row.addView(text2);
                TextView text3 = new TextView(getActivity());
                text3.setText(key.get("Open"));
                text3.setGravity(Gravity.CENTER);
                row.addView(text3);
                TextView text4 = new TextView(getActivity());
                text4.setText(key.get("Close"));
                text4.setGravity(Gravity.CENTER);
                ArrayList<TextView> rowText = new ArrayList<TextView>();
                rowText.add(text1);
                rowText.add(text2);
                rowText.add(text3);
                rowText.add(text4);
                if (key.get("Status").equals("Not Started")) {
                    for (TextView tv : rowText) {
                        tv.setTextColor(Color.RED);
                        tv.setTypeface(null, Typeface.BOLD);
                    }
                } else if (key.get("Status").equals("Submitted")) {
                    for (TextView tv : rowText) {
                        tv.setTextColor(Color.GREEN);
                        tv.setTypeface(null, Typeface.BOLD);
                    }
                } else if (key.get("Status").equals("Returned")) {
                    for (TextView tv : rowText) {
                        tv.setTextColor(Color.GRAY);
                    }
                }


                row.addView(text4);
                rootView.addView(row);

            }

            return rootView;
        }

        public void setAssignments(ArrayList data) {
            assignments = data;
        }
    }

    private long currentTime = Calendar.getInstance().getTimeInMillis();
    long fiveMins = 300000;

    public void synchronizeCalendar() {
        CalendarActivity cal = new CalendarActivity();
        cal.setContext(this.getApplicationContext());
        Calendar c = Calendar.getInstance();

        Iterator itr = (Iterator) myMap.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry pairs = (Map.Entry) itr.next();

            String course = String.valueOf(pairs.getKey());
            ArrayList<HashMap> assignments = myMap.get(course);

            for (HashMap<String, String> key : assignments) {
                String title, description = null, location = null;
                long startDate = 0, endDate = 0, idCalendar = 1;

                title = course + ": " + key.get("Title");
                try {

                    startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(key.get("Close")).getTime();
                    if (startDate < currentTime) {
                        break;
                    }
                    endDate = startDate + fiveMins;
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                CalendarEvent calEvent = new CalendarEvent(title, description, location, startDate, endDate, idCalendar);
                cal.addEvent(calEvent);
            }
        }
    }

    public void setAlarm(String text, String notificationTime) {
        alarm.setAlarm(getApplicationContext(), notificationTime);
    }

    private void setAssignmentAlarm() {

        Iterator itr = (Iterator) myMap.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry pairs = (Map.Entry) itr.next();

            String course = String.valueOf(pairs.getKey());
            ArrayList<HashMap> assignments = myMap.get(course);

            for (HashMap<String, String> key : assignments) {
                String title, description = null, location = null;

                long dueDate = 0;

                title = course + ": " + key.get("Title");
                try {

                    dueDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(key.get("Close")).getTime();
                    String dueDateString = key.get("Close");
                    if (dueDate < currentTime) {
                        break;
                    } else {
                        Toast toast = Toast.makeText(HomePageActivity.this, "Added " + course + ": " + title + " to notification", 300000);
                        toast.show();
                        setAlarm(course + ": " + title + " is due at " + dueDateString, dueDateString);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }

    }
}
