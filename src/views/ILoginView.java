package views;

import java.util.Map;

public interface ILoginView {
	public void advance();
	public void login_error();
    public void setData(Map map);
    public void showProgressBar();
    public void hideProgressBar();
}
