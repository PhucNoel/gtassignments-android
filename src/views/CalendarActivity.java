package views;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;

/**
 * Created with IntelliJ IDEA.
 * User: Phuc Noel
 * Date: 10/28/13
 * Time: 1:11 AM
 */
public class CalendarActivity {
    private String baseUri;
    private Context ctx;

    public void addEvent(CalendarEvent evt) {
        ContentResolver cr = ctx.getContentResolver();
        try {
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, CalendarEvent.toICSContentValues(evt));
            System.out.println("Event URI [" + uri + "]");
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public void setContext(Context context) {
        this.ctx = context;
        this.baseUri = getCalendarUriBase();
    }

    private Uri getCalendarUri(String path) {
        return Uri.parse(baseUri + "/" + path);
    }

    private String getCalendarUriBase() {
        String calendarUriBase = null;
        Uri calendars = Uri.parse("content://calendar/calendars");
        Cursor managedCursor = null;
        try {
            managedCursor = ctx.getContentResolver().query(calendars, null, null, null, null);
        } catch (Exception e) {
            // e.printStackTrace();
        }

        if (managedCursor != null) {
            calendarUriBase = "content://calendar/";
        } else {
            calendars = Uri.parse("content://com.android.calendar/calendars");
            try {
                managedCursor = ctx.getContentResolver().query(calendars, null, null, null, null);
            } catch (Exception e) {
                // e.printStackTrace();
            }

            if (managedCursor != null) {
                calendarUriBase = "content://com.android.calendar/";
            }

        }

        return calendarUriBase;
    }
}
